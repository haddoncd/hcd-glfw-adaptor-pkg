#ifdef glfwCallbacks_cpp
#error Multiple inclusion
#endif
#define glfwCallbacks_cpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef utf8_hpp
#error "Please include utf8.hpp before this file"
#endif

#ifndef openglGame_common_hpp
#error "Please include openglGame_common.hpp before this file"
#endif

#ifndef _glfw3_h_
#error "Please include GLFW/glfw3.h before this file"
#endif

#include <cstdio>

namespace glfwCallbacks
{
  // TODO: Multi-window support?
  GLFWwindow              *window;

  double                   glfwTime_seconds;

  bool                     mouseCursorPresent;
  openglGame::ButtonState  mouseButtons[openglGame::MouseButton::ENUM_COUNT];
  double                   mouseScroll;

  openglGame::ButtonState  keyboardKeys[openglGame::KeyboardKey::ENUM_COUNT];

  namespace textInput
  {
    constexpr size_t MAX_BYTES = 64;

    bool writeTextB;
    u32  byteCount;
    bool overflowed;
    u8   textBytesA_utf8[MAX_BYTES];
    u8   textBytesB_utf8[MAX_BYTES];
  }

  void updateButtonState(openglGame::ButtonState *button_state, int glfw_action)
  {
    if (button_state)
    {
      // Don't register a transition if we only receive half of the
      // PRESS/RELEASE pair
      if (glfw_action == (button_state->isDown ? GLFW_RELEASE : GLFW_PRESS))
      {
        ++button_state->transitionCount;
      }
      button_state->isDown = (glfw_action == GLFW_PRESS) || (glfw_action == GLFW_REPEAT);
    }
  }

  void cursorEnterCallback(GLFWwindow *event_window, int entered)
  {
    if (event_window == window)
    {
      mouseCursorPresent = (entered == GL_TRUE);
    }
  }

  void mouseButtonCallback(GLFWwindow *event_window, int glfw_button, int glfw_action, int glfw_mod_flags)
  {
    if (event_window == window)
    {
      openglGame::ButtonState *button_state = 0;
      switch (glfw_button)
      {
        case GLFW_MOUSE_BUTTON_LEFT:
        {
          button_state = &mouseButtons[openglGame::MouseButton::LEFT];
        } break;
        case GLFW_MOUSE_BUTTON_RIGHT:
        {
          button_state = &mouseButtons[openglGame::MouseButton::RIGHT];
        } break;
        case GLFW_MOUSE_BUTTON_MIDDLE:
        {
          button_state = &mouseButtons[openglGame::MouseButton::MIDDLE];
        } break;
      }

      updateButtonState(button_state, glfw_action);
    }
  }

  void scrollCallback(GLFWwindow *event_window, double x_offset, double y_offset)
  {
    if (event_window == window)
    {
      mouseScroll += y_offset;
    }
  }

  void keyCallback(GLFWwindow *event_window, int glfw_key, int scancode, int glfw_action, int glfw_mod_flags)
  {
    if (event_window == window)
    {
      openglGame::ButtonState *button_state = 0;
      switch (glfw_key)
      {
        case GLFW_KEY_SPACE:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::SPACE];
        } break;
        case GLFW_KEY_APOSTROPHE:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::APOSTROPHE];
        } break;
        case GLFW_KEY_COMMA:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::COMMA];
        } break;
        case GLFW_KEY_MINUS:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::MINUS];
        } break;
        case GLFW_KEY_PERIOD:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::PERIOD];
        } break;
        case GLFW_KEY_SLASH:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::SLASH];
        } break;
        case GLFW_KEY_0:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::NUM_0];
        } break;
        case GLFW_KEY_1:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::NUM_1];
        } break;
        case GLFW_KEY_2:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::NUM_2];
        } break;
        case GLFW_KEY_3:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::NUM_3];
        } break;
        case GLFW_KEY_4:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::NUM_4];
        } break;
        case GLFW_KEY_5:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::NUM_5];
        } break;
        case GLFW_KEY_6:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::NUM_6];
        } break;
        case GLFW_KEY_7:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::NUM_7];
        } break;
        case GLFW_KEY_8:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::NUM_8];
        } break;
        case GLFW_KEY_9:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::NUM_9];
        } break;
        case GLFW_KEY_SEMICOLON:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::SEMICOLON];
        } break;
        case GLFW_KEY_EQUAL:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::EQUAL];
        } break;
        case GLFW_KEY_A:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::A];
        } break;
        case GLFW_KEY_B:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::B];
        } break;
        case GLFW_KEY_C:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::C];
        } break;
        case GLFW_KEY_D:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::D];
        } break;
        case GLFW_KEY_E:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::E];
        } break;
        case GLFW_KEY_F:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::F];
        } break;
        case GLFW_KEY_G:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::G];
        } break;
        case GLFW_KEY_H:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::H];
        } break;
        case GLFW_KEY_I:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::I];
        } break;
        case GLFW_KEY_J:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::J];
        } break;
        case GLFW_KEY_K:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::K];
        } break;
        case GLFW_KEY_L:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::L];
        } break;
        case GLFW_KEY_M:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::M];
        } break;
        case GLFW_KEY_N:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::N];
        } break;
        case GLFW_KEY_O:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::O];
        } break;
        case GLFW_KEY_P:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::P];
        } break;
        case GLFW_KEY_Q:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::Q];
        } break;
        case GLFW_KEY_R:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::R];
        } break;
        case GLFW_KEY_S:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::S];
        } break;
        case GLFW_KEY_T:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::T];
        } break;
        case GLFW_KEY_U:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::U];
        } break;
        case GLFW_KEY_V:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::V];
        } break;
        case GLFW_KEY_W:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::W];
        } break;
        case GLFW_KEY_X:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::X];
        } break;
        case GLFW_KEY_Y:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::Y];
        } break;
        case GLFW_KEY_Z:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::Z];
        } break;
        case GLFW_KEY_LEFT_BRACKET:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::LEFT_BRACKET];
        } break;
        case GLFW_KEY_BACKSLASH:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::BACKSLASH];
        } break;
        case GLFW_KEY_RIGHT_BRACKET:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::RIGHT_BRACKET];
        } break;
        case GLFW_KEY_GRAVE_ACCENT:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::GRAVE_ACCENT];
        } break;
        case GLFW_KEY_ESCAPE:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::ESCAPE];
        } break;
        case GLFW_KEY_ENTER:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::ENTER];
        } break;
        case GLFW_KEY_TAB:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::TAB];
        } break;
        case GLFW_KEY_BACKSPACE:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::BACKSPACE];
        } break;
        case GLFW_KEY_INSERT:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::INSERT];
        } break;
        case GLFW_KEY_DELETE:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::DEL];
        } break;
        case GLFW_KEY_RIGHT:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::RIGHT_ARROW];
        } break;
        case GLFW_KEY_LEFT:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::LEFT_ARROW];
        } break;
        case GLFW_KEY_DOWN:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::DOWN_ARROW];
        } break;
        case GLFW_KEY_UP:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::UP_ARROW];
        } break;
        case GLFW_KEY_PAGE_UP:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::PAGE_UP];
        } break;
        case GLFW_KEY_PAGE_DOWN:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::PAGE_DOWN];
        } break;
        case GLFW_KEY_HOME:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::HOME];
        } break;
        case GLFW_KEY_END:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::END];
        } break;
        case GLFW_KEY_F1:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::F1];
        } break;
        case GLFW_KEY_F2:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::F2];
        } break;
        case GLFW_KEY_F3:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::F3];
        } break;
        case GLFW_KEY_F4:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::F4];
        } break;
        case GLFW_KEY_F5:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::F5];
        } break;
        case GLFW_KEY_F6:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::F6];
        } break;
        case GLFW_KEY_F7:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::F7];
        } break;
        case GLFW_KEY_F8:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::F8];
        } break;
        case GLFW_KEY_F9:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::F9];
        } break;
        case GLFW_KEY_F10:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::F10];
        } break;
        case GLFW_KEY_F11:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::F11];
        } break;
        case GLFW_KEY_F12:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::F12];
        } break;
        case GLFW_KEY_LEFT_SHIFT:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::LEFT_SHIFT];
        } break;
        case GLFW_KEY_LEFT_CONTROL:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::LEFT_CONTROL];
        } break;
        case GLFW_KEY_LEFT_ALT:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::LEFT_ALT];
        } break;
        case GLFW_KEY_RIGHT_SHIFT:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::RIGHT_SHIFT];
        } break;
        case GLFW_KEY_RIGHT_CONTROL:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::RIGHT_CONTROL];
        } break;
        case GLFW_KEY_RIGHT_ALT:
        {
          button_state = &keyboardKeys[openglGame::KeyboardKey::RIGHT_ALT];
        } break;
      }

      updateButtonState(button_state, glfw_action);
    }
  }

  void charCallback(GLFWwindow *event_window, unsigned int entered)
  {
    if (event_window == window && !textInput::overflowed)
    {
      utf8_Codepoint codepoint = utf8_codepoint(entered);

      if (textInput::byteCount + codepoint.byteCount < textInput::MAX_BYTES)
      {
        u8 *bytes_utf8 = textInput::writeTextB ?
                         textInput::textBytesB_utf8 : textInput::textBytesA_utf8;
        for (u32 i = 0; i < codepoint.byteCount; ++i)
        {
          bytes_utf8[textInput::byteCount++] = codepoint.bytes[i];
        }
      }
      else
      {
        printf("Warning: text input overflow, data lost\n");
        textInput::overflowed = true;
      }
    }
  }

  openglGame_CLIPBOARD_COPY_FUNCTION(copy)
  {
    glfwSetClipboardString(window, text);
  }

  openglGame_CLIPBOARD_PASTE_FUNCTION(paste)
  {
    return glfwGetClipboardString(window);
  }

  void attach(GLFWwindow *glfw_window)
  {
    window = glfw_window;

    glfwSetCursorEnterCallback(window, cursorEnterCallback);
    glfwSetMouseButtonCallback(window, mouseButtonCallback);
    glfwSetScrollCallback(window, scrollCallback);
    glfwSetKeyCallback(window, keyCallback);
    glfwSetCharCallback(window, charCallback);
  }

  void getInput(openglGame::Input *input)
  {
    {
      V2<int> tmp;

      glfwGetFramebufferSize(window, &tmp.x, &tmp.y);

      input->windowSize_pixels = v2<u32>(tmp);
    }

    {
      V2<int> tmp;

      glfwGetWindowSize(window, &tmp.x, &tmp.y);

      input->windowSize_points = v2<f64>(tmp);
    }

    for (int button = 0;
         button < openglGame::MouseButton::ENUM_COUNT;
         ++button)
    {
      input->mouse.buttons[button] = mouseButtons[button];
      mouseButtons[button].transitionCount = 0;
    }

    input->mouse.cursorPresent = mouseCursorPresent;
    if (input->mouse.cursorPresent)
    {
      V2<f64> mouse_tmp;
      glfwGetCursorPos(window,
                       &mouse_tmp.x,
                       &mouse_tmp.y);

      input->mouse.cursorPos_points =
        v2(mouse_tmp.x,
           input->windowSize_points.y - mouse_tmp.y);
    }
    else
    {
      input->mouse.cursorPos_points = v2(0.0);
    }

    input->mouse.scroll_wheelClicks = mouseScroll;
    mouseScroll = 0.0;

    for (int key = 0;
         key < openglGame::KeyboardKey::ENUM_COUNT;
         ++key)
    {
      input->keyboard.keys[key] = keyboardKeys[key];
      keyboardKeys[key].transitionCount = 0;
    }

    input->keyboard.text.overflowed = textInput::overflowed;
    textInput::overflowed = false;

    input->keyboard.text.bytes_utf8 = textInput::writeTextB ?
                                      textInput::textBytesB_utf8 :
                                      textInput::textBytesA_utf8;

    zeroArray(textInput::writeTextB ?
              textInput::textBytesA_utf8 :
              textInput::textBytesB_utf8);
    textInput::byteCount = 0;

    textInput::writeTextB = !textInput::writeTextB;

    {
      double prev_glfw_time = glfwTime_seconds;
      glfwTime_seconds = glfwGetTime();
      input->timeStep_seconds = glfwTime_seconds - prev_glfw_time;
    }
  }
}
